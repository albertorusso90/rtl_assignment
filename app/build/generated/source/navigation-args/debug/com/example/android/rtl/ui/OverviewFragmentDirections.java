package com.example.android.rtl.ui;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import com.example.android.rtl.R;
import com.example.android.rtl.domain.Article;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class OverviewFragmentDirections {
  private OverviewFragmentDirections() {
  }

  @NonNull
  public static ActionOverviewFragmentToDetailsFragment actionOverviewFragmentToDetailsFragment(@NonNull Article selectedArticle) {
    return new ActionOverviewFragmentToDetailsFragment(selectedArticle);
  }

  public static class ActionOverviewFragmentToDetailsFragment implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionOverviewFragmentToDetailsFragment(@NonNull Article selectedArticle) {
      if (selectedArticle == null) {
        throw new IllegalArgumentException("Argument \"selectedArticle\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("selectedArticle", selectedArticle);
    }

    @NonNull
    public ActionOverviewFragmentToDetailsFragment setSelectedArticle(@NonNull Article selectedArticle) {
      if (selectedArticle == null) {
        throw new IllegalArgumentException("Argument \"selectedArticle\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("selectedArticle", selectedArticle);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("selectedArticle")) {
        Article selectedArticle = (Article) arguments.get("selectedArticle");
        if (Parcelable.class.isAssignableFrom(Article.class) || selectedArticle == null) {
          __result.putParcelable("selectedArticle", Parcelable.class.cast(selectedArticle));
        } else if (Serializable.class.isAssignableFrom(Article.class)) {
          __result.putSerializable("selectedArticle", Serializable.class.cast(selectedArticle));
        } else {
          throw new UnsupportedOperationException(Article.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_overviewFragment_to_detailsFragment;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public Article getSelectedArticle() {
      return (Article) arguments.get("selectedArticle");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionOverviewFragmentToDetailsFragment that = (ActionOverviewFragmentToDetailsFragment) object;
      if (arguments.containsKey("selectedArticle") != that.arguments.containsKey("selectedArticle")) {
        return false;
      }
      if (getSelectedArticle() != null ? !getSelectedArticle().equals(that.getSelectedArticle()) : that.getSelectedArticle() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getSelectedArticle() != null ? getSelectedArticle().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionOverviewFragmentToDetailsFragment(actionId=" + getActionId() + "){"
          + "selectedArticle=" + getSelectedArticle()
          + "}";
    }
  }
}
