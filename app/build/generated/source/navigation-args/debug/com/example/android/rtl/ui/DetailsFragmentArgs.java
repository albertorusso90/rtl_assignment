package com.example.android.rtl.ui;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.navigation.NavArgs;
import com.example.android.rtl.domain.Article;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class DetailsFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private DetailsFragmentArgs() {
  }

  private DetailsFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static DetailsFragmentArgs fromBundle(@NonNull Bundle bundle) {
    DetailsFragmentArgs __result = new DetailsFragmentArgs();
    bundle.setClassLoader(DetailsFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("selectedArticle")) {
      Article selectedArticle;
      if (Parcelable.class.isAssignableFrom(Article.class) || Serializable.class.isAssignableFrom(Article.class)) {
        selectedArticle = (Article) bundle.get("selectedArticle");
      } else {
        throw new UnsupportedOperationException(Article.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      if (selectedArticle == null) {
        throw new IllegalArgumentException("Argument \"selectedArticle\" is marked as non-null but was passed a null value.");
      }
      __result.arguments.put("selectedArticle", selectedArticle);
    } else {
      throw new IllegalArgumentException("Required argument \"selectedArticle\" is missing and does not have an android:defaultValue");
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Article getSelectedArticle() {
    return (Article) arguments.get("selectedArticle");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("selectedArticle")) {
      Article selectedArticle = (Article) arguments.get("selectedArticle");
      if (Parcelable.class.isAssignableFrom(Article.class) || selectedArticle == null) {
        __result.putParcelable("selectedArticle", Parcelable.class.cast(selectedArticle));
      } else if (Serializable.class.isAssignableFrom(Article.class)) {
        __result.putSerializable("selectedArticle", Serializable.class.cast(selectedArticle));
      } else {
        throw new UnsupportedOperationException(Article.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    DetailsFragmentArgs that = (DetailsFragmentArgs) object;
    if (arguments.containsKey("selectedArticle") != that.arguments.containsKey("selectedArticle")) {
      return false;
    }
    if (getSelectedArticle() != null ? !getSelectedArticle().equals(that.getSelectedArticle()) : that.getSelectedArticle() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getSelectedArticle() != null ? getSelectedArticle().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "DetailsFragmentArgs{"
        + "selectedArticle=" + getSelectedArticle()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(DetailsFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder(@NonNull Article selectedArticle) {
      if (selectedArticle == null) {
        throw new IllegalArgumentException("Argument \"selectedArticle\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("selectedArticle", selectedArticle);
    }

    @NonNull
    public DetailsFragmentArgs build() {
      DetailsFragmentArgs result = new DetailsFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setSelectedArticle(@NonNull Article selectedArticle) {
      if (selectedArticle == null) {
        throw new IllegalArgumentException("Argument \"selectedArticle\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("selectedArticle", selectedArticle);
      return this;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public Article getSelectedArticle() {
      return (Article) arguments.get("selectedArticle");
    }
  }
}
