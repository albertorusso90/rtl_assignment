// Code generated by moshi-kotlin-codegen. Do not edit.
package com.example.android.rtl.network

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonDataException
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import java.lang.NullPointerException
import kotlin.String
import kotlin.collections.List

class NetworkArticleContainerJsonAdapter(moshi: Moshi) : JsonAdapter<NetworkArticleContainer>() {
    private val options: JsonReader.Options = JsonReader.Options.of("articles")

    private val listOfNetworkArticleAdapter: JsonAdapter<List<NetworkArticle>> =
            moshi.adapter<List<NetworkArticle>>(Types.newParameterizedType(List::class.java, NetworkArticle::class.java)).nonNull()

    override fun toString(): String = "GeneratedJsonAdapter(NetworkArticleContainer)"

    override fun fromJson(reader: JsonReader): NetworkArticleContainer {
        var articles: List<NetworkArticle>? = null
        reader.beginObject()
        while (reader.hasNext()) {
            when (reader.selectName(options)) {
                0 -> articles = listOfNetworkArticleAdapter.fromJson(reader) ?: throw JsonDataException("Non-null value 'articles' was null at ${reader.path}")
                -1 -> {
                    // Unknown name, skip it.
                    reader.skipName()
                    reader.skipValue()
                }
            }
        }
        reader.endObject()
        var result = NetworkArticleContainer(
                articles = articles ?: throw JsonDataException("Required property 'articles' missing at ${reader.path}"))
        return result
    }

    override fun toJson(writer: JsonWriter, value: NetworkArticleContainer?) {
        if (value == null) {
            throw NullPointerException("value was null! Wrap in .nullSafe() to write nullable values.")
        }
        writer.beginObject()
        writer.name("articles")
        listOfNetworkArticleAdapter.toJson(writer, value.articles)
        writer.endObject()
    }
}
