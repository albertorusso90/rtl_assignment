package com.example.android.rtl.databinding;
import com.example.android.rtl.R;
import com.example.android.rtl.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ArticleItemBindingImpl extends ArticleItemBinding implements com.example.android.rtl.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final com.google.android.material.card.MaterialCardView mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback1;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ArticleItemBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 4, sIncludes, sViewsWithIds));
    }
    private ArticleItemBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.view.View) bindings[3]
            , (android.widget.TextView) bindings[2]
            , (android.widget.ImageView) bindings[1]
            );
        this.clickableOverlay.setTag(null);
        this.mboundView0 = (com.google.android.material.card.MaterialCardView) bindings[0];
        this.mboundView0.setTag(null);
        this.title.setTag(null);
        this.videoThumbnail.setTag(null);
        setRootTag(root);
        // listeners
        mCallback1 = new com.example.android.rtl.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.article == variableId) {
            setArticle((com.example.android.rtl.domain.Article) variable);
        }
        else if (BR.articleCallback == variableId) {
            setArticleCallback((com.example.android.rtl.ui.ArticleClick) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setArticle(@Nullable com.example.android.rtl.domain.Article Article) {
        this.mArticle = Article;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.article);
        super.requestRebind();
    }
    public void setArticleCallback(@Nullable com.example.android.rtl.ui.ArticleClick ArticleCallback) {
        this.mArticleCallback = ArticleCallback;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.articleCallback);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.example.android.rtl.domain.Article article = mArticle;
        java.lang.String articleUrlToImage = null;
        com.example.android.rtl.ui.ArticleClick articleCallback = mArticleCallback;
        java.lang.String articleTitle = null;

        if ((dirtyFlags & 0x5L) != 0) {



                if (article != null) {
                    // read article.urlToImage
                    articleUrlToImage = article.getUrlToImage();
                    // read article.title
                    articleTitle = article.getTitle();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.clickableOverlay.setOnClickListener(mCallback1);
        }
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.title, articleTitle);
            com.example.android.rtl.util.BindingAdaptersKt.setImageUrl(this.videoThumbnail, articleUrlToImage);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // article
        com.example.android.rtl.domain.Article article = mArticle;
        // articleCallback
        com.example.android.rtl.ui.ArticleClick articleCallback = mArticleCallback;
        // articleCallback != null
        boolean articleCallbackJavaLangObjectNull = false;



        articleCallbackJavaLangObjectNull = (articleCallback) != (null);
        if (articleCallbackJavaLangObjectNull) {



            articleCallback.onClick(article);
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): article
        flag 1 (0x2L): articleCallback
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}