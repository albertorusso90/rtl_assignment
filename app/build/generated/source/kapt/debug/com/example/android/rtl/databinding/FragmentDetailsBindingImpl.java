package com.example.android.rtl.databinding;
import com.example.android.rtl.R;
import com.example.android.rtl.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentDetailsBindingImpl extends FragmentDetailsBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.left_well, 1);
        sViewsWithIds.put(R.id.right_well, 2);
        sViewsWithIds.put(R.id.title, 3);
        sViewsWithIds.put(R.id.author, 4);
        sViewsWithIds.put(R.id.publishedAt, 5);
        sViewsWithIds.put(R.id.description, 6);
        sViewsWithIds.put(R.id.content, 7);
        sViewsWithIds.put(R.id.buttonViewArticle, 8);
        sViewsWithIds.put(R.id.video_thumbnail, 9);
    }
    // views
    @NonNull
    private final com.google.android.material.card.MaterialCardView mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentDetailsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 10, sIncludes, sViewsWithIds));
    }
    private FragmentDetailsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[4]
            , (android.widget.Button) bindings[8]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[6]
            , (androidx.constraintlayout.widget.Guideline) bindings[1]
            , (android.widget.TextView) bindings[5]
            , (androidx.constraintlayout.widget.Guideline) bindings[2]
            , (android.widget.TextView) bindings[3]
            , (android.widget.ImageView) bindings[9]
            );
        this.mboundView0 = (com.google.android.material.card.MaterialCardView) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.details == variableId) {
            setDetails((com.example.android.rtl.domain.Article) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setDetails(@Nullable com.example.android.rtl.domain.Article Details) {
        this.mDetails = Details;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): details
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}