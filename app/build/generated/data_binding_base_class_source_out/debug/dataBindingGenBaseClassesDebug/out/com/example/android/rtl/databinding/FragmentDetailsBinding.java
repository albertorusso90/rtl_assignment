package com.example.android.rtl.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Guideline;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.example.android.rtl.domain.Article;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentDetailsBinding extends ViewDataBinding {
  @NonNull
  public final TextView author;

  @NonNull
  public final Button buttonViewArticle;

  @NonNull
  public final TextView content;

  @NonNull
  public final TextView description;

  @NonNull
  public final Guideline leftWell;

  @NonNull
  public final TextView publishedAt;

  @NonNull
  public final Guideline rightWell;

  @NonNull
  public final TextView title;

  @NonNull
  public final ImageView videoThumbnail;

  @Bindable
  protected Article mDetails;

  protected FragmentDetailsBinding(Object _bindingComponent, View _root, int _localFieldCount,
      TextView author, Button buttonViewArticle, TextView content, TextView description,
      Guideline leftWell, TextView publishedAt, Guideline rightWell, TextView title,
      ImageView videoThumbnail) {
    super(_bindingComponent, _root, _localFieldCount);
    this.author = author;
    this.buttonViewArticle = buttonViewArticle;
    this.content = content;
    this.description = description;
    this.leftWell = leftWell;
    this.publishedAt = publishedAt;
    this.rightWell = rightWell;
    this.title = title;
    this.videoThumbnail = videoThumbnail;
  }

  public abstract void setDetails(@Nullable Article details);

  @Nullable
  public Article getDetails() {
    return mDetails;
  }

  @NonNull
  public static FragmentDetailsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_details, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentDetailsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentDetailsBinding>inflateInternal(inflater, com.example.android.rtl.R.layout.fragment_details, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentDetailsBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_details, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentDetailsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentDetailsBinding>inflateInternal(inflater, com.example.android.rtl.R.layout.fragment_details, null, false, component);
  }

  public static FragmentDetailsBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentDetailsBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentDetailsBinding)bind(component, view, com.example.android.rtl.R.layout.fragment_details);
  }
}
