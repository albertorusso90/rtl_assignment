package com.example.android.rtl.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.example.android.rtl.domain.Article;
import com.example.android.rtl.ui.ArticleClick;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class ArticleItemBinding extends ViewDataBinding {
  @NonNull
  public final View clickableOverlay;

  @NonNull
  public final TextView title;

  @NonNull
  public final ImageView videoThumbnail;

  @Bindable
  protected Article mArticle;

  @Bindable
  protected ArticleClick mArticleCallback;

  protected ArticleItemBinding(Object _bindingComponent, View _root, int _localFieldCount,
      View clickableOverlay, TextView title, ImageView videoThumbnail) {
    super(_bindingComponent, _root, _localFieldCount);
    this.clickableOverlay = clickableOverlay;
    this.title = title;
    this.videoThumbnail = videoThumbnail;
  }

  public abstract void setArticle(@Nullable Article article);

  @Nullable
  public Article getArticle() {
    return mArticle;
  }

  public abstract void setArticleCallback(@Nullable ArticleClick articleCallback);

  @Nullable
  public ArticleClick getArticleCallback() {
    return mArticleCallback;
  }

  @NonNull
  public static ArticleItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.article_item, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static ArticleItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<ArticleItemBinding>inflateInternal(inflater, com.example.android.rtl.R.layout.article_item, root, attachToRoot, component);
  }

  @NonNull
  public static ArticleItemBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.article_item, null, false, component)
   */
  @NonNull
  @Deprecated
  public static ArticleItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<ArticleItemBinding>inflateInternal(inflater, com.example.android.rtl.R.layout.article_item, null, false, component);
  }

  public static ArticleItemBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static ArticleItemBinding bind(@NonNull View view, @Nullable Object component) {
    return (ArticleItemBinding)bind(component, view, com.example.android.rtl.R.layout.article_item);
  }
}
