package com.example.android.rtl.network;

import java.lang.System;

/**
 * Main entry point for network access. Call like `ArticlesNetwork.articles.getArticles()`
 */
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0019\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0016\u0010\b\u001a\n \u0005*\u0004\u0018\u00010\t0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"}, d2 = {"Lcom/example/android/rtl/network/ArticlesNetwork;", "", "()V", "articles", "Lcom/example/android/rtl/network/ArticlesService;", "kotlin.jvm.PlatformType", "getArticles", "()Lcom/example/android/rtl/network/ArticlesService;", "retrofit", "Lretrofit2/Retrofit;", "app_debug"})
public final class ArticlesNetwork {
    private static final retrofit2.Retrofit retrofit = null;
    private static final com.example.android.rtl.network.ArticlesService articles = null;
    public static final com.example.android.rtl.network.ArticlesNetwork INSTANCE = null;
    
    public final com.example.android.rtl.network.ArticlesService getArticles() {
        return null;
    }
    
    private ArticlesNetwork() {
        super();
    }
}