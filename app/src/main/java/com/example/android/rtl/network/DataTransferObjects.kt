/*
 * Copyright (C) 2019 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.rtl.network

import com.example.android.rtl.domain.Article
import com.squareup.moshi.JsonClass

/**
 * DataTransferObjects go in this file. These are responsible for parsing responses from the server
 * or formatting objects to send to the server. You should convert these to domain objects before
 * using them.
 *
 * @see domain package for
 */

/**
 * ArticleHolder holds a list of Articles.
 *
 * This is to parse first level of our network result which looks like
 *
 * {
 *   "articles": []
 * }
 */
@JsonClass(generateAdapter = true)
data class NetworkArticleContainer(val articles: List<NetworkArticle>)

/**
 * Articles represent an article that can be played.
 */
@JsonClass(generateAdapter = true)
data class NetworkArticle(
        val author: String?,
        val title: String,
        val description: String?,
        val publishedAt: String,
        val content: String?,
        val url: String,
        val urlToImage: String?)

/**
 * Convert Network results to database objects
 */
fun NetworkArticleContainer.asDomainModel(): List<Article> {
    return articles.map {
        Article(
                author = it.author,
                title = it.title,
                description = it.description,
                publishedAt = it.publishedAt,
                content = it.content,
                url = it.url,
                urlToImage = it.urlToImage)
    }
}
