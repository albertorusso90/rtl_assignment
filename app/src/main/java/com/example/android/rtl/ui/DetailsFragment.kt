package com.example.android.rtl.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.android.rtl.R
import com.example.android.rtl.databinding.FragmentDetailsBinding
import com.example.android.rtl.util.setImageUrl

/**
 * Created by Alberto Russo on 24/06/2020.
 */
class DetailsFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding: FragmentDetailsBinding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_details,
                container,
                false)

        val article = DetailsFragmentArgs.fromBundle(arguments!!).selectedArticle

        setImageUrl(binding.videoThumbnail, article.urlToImage)
        binding.title.setText(article.title)
        binding.author.setText(article.author)
        binding.publishedAt.setText(article.publishedAt)
        binding.description.setText(article.description)
        binding.content.setText(article.content)

        binding.buttonViewArticle.setOnClickListener{
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(article.url))
            startActivity(intent)
        }

        return binding.root
    }

}